﻿using WebDriverFeatures.Pages.Jsbin;
using Xunit;

namespace WebDriverFeatures.Tests
{
    public class Frames : BaseTest
    {
        private readonly string JSBinPageUrl = "http://jsbin.com/?html,output";
        private readonly string HTMLToSendInBody = "<input id='test'/>";
        private readonly string TextToSendInTestInput = "любой произвольный текст";
        [Fact]
        public void FramesTest()
        {
            Browser.OpenURL(JSBinPageUrl);
            MainPage mainPage = new MainPage(Browser);
            mainPage.SendTextInBody(HTMLToSendInBody);
            mainPage.SwitchToIFrame(mainPage.OuterFrame);
            mainPage.SwitchToIFrame(mainPage.InnerFrame);
            mainPage.SendKeys(mainPage.TestInput, TextToSendInTestInput);
        }
    }
}
