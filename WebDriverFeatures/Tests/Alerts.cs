using System;
using WebDriverFeatures.Pages.Toolsqa;
using Xunit;

namespace WebDriverFeatures.Tests
{
    public class Alerts : BaseTest
    {
        private readonly string AlertsPageUrl = "https://www.toolsqa.com/handling-alerts-using-selenium-webdriver/";
        private readonly string TextToSendInPromptAlert = "Great site";
        [Fact]
        public void AlertsTest()
        {
            Browser.OpenURL(AlertsPageUrl);
            AlertsPage alertsPage = new AlertsPage(Browser);
            alertsPage.AcceptCookie();
            
            //Scrolling
            Browser.ScrollToBottom();
            Browser.ScrollToTop();
            
            alertsPage.SimpleAlertButton.Click();
            Console.WriteLine(alertsPage.GetAlert().Text);
            alertsPage.GetAlert().Accept();
            alertsPage.ConfirmAlertButton.Click();
            Console.WriteLine(alertsPage.GetAlert().Text);
            alertsPage.GetAlert().Accept();
            alertsPage.PromptAlertButton.Click();
            Console.WriteLine(alertsPage.GetAlert().Text);
            alertsPage.GetAlert().SendKeys(TextToSendInPromptAlert);
            alertsPage.GetAlert().Accept();
        }
    }
}
