﻿using Xunit;
using System;
using WebDriverFeatures.Pages.Toolsqa;
using OpenQA.Selenium;

namespace WebDriverFeatures.Tests
{
    public class Assertions : BaseTest
    {
        private readonly string AlertsPageUrl = "https://www.toolsqa.com/handling-alerts-using-selenium-webdriver/";
        [Fact]
        public void AssertionsTest()
        {
            Browser.OpenURL(AlertsPageUrl);
            AlertsPage alertsPage = new AlertsPage(Browser);
            
            //Equality Asserts
            IWebElement originalWebElement = alertsPage.AcceptCookieButton;
            string acceptCookieButtonText = "ACCEPT";
            Assert.Equal(acceptCookieButtonText, originalWebElement.Text);
            acceptCookieButtonText = "Accept";
            Assert.NotEqual(acceptCookieButtonText, originalWebElement.Text);

            //Identity Asserts
            IWebElement sameWebElement = alertsPage.AcceptCookieButton;
            Assert.NotSame(sameWebElement, originalWebElement);
            IWebElement copiedWebElement = originalWebElement;
            Assert.Same(copiedWebElement, originalWebElement);

            //Condition Asserts
            acceptCookieButtonText = "ACCEPT";
            Assert.True(originalWebElement.Text == acceptCookieButtonText);
            acceptCookieButtonText = "Accept";
            Assert.False(originalWebElement.Text == acceptCookieButtonText);
            
            Assert.NotEmpty(acceptCookieButtonText);
            acceptCookieButtonText = "";
            Assert.Empty(acceptCookieButtonText);
            
            Assert.NotNull(acceptCookieButtonText);
            acceptCookieButtonText = null;
            Assert.Null(acceptCookieButtonText);

            //Exception Asserts
            Assert.Throws<DivideByZeroException>(DivideByZero);
        }

        private static void DivideByZero()
        {
            throw new DivideByZeroException();
        }
    }
}
