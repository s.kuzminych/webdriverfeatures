﻿using WebDriverFeatures.Pages.Onliner;
using Xunit;

namespace WebDriverFeatures.Tests
{
    public class Waits : BaseTest
    {
        private readonly string TVCatalogPageUrl = "https://catalog.onliner.by/tv/";
        [Fact]
        public void WaitsTest()
        {
            Browser.OpenURL(TVCatalogPageUrl);
            TVCatalogPage tvCatalogPage = new TVCatalogPage(Browser);
            tvCatalogPage.CompareCheckboxes[0].Click();
            tvCatalogPage.CompareCheckboxes[1].Click();
            ComparePage comparePage = tvCatalogPage.GoToComparePage();
            comparePage.MoveMouseToElement(comparePage.SizeParameterCell);
            comparePage.WaitUntilElementIsDisplayed(comparePage.SizeParameterTip);
            comparePage.SizeParameterTip.Click();
            comparePage.WaitUntilElementIsDisplayed(comparePage.Tip);
            comparePage.SizeParameterTip.Click();
            comparePage.WaitUntilElementIsNotDisplayed(comparePage.Tip);
            comparePage.DeleteElementButtons[0].Click();
            comparePage.WaitUntilElementIsDisplayed(comparePage.Title);
        }
    }
}
