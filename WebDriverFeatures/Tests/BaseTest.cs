﻿using System;
using WebDriverFeatures.Browsers;

namespace WebDriverFeatures.Tests
{
    public abstract class BaseTest : IDisposable
    {
        protected Browser Browser { get; }
        protected BaseTest()
        {
            Browser = Browser.GetInstance();
        }
        public void Dispose()
        {
            Browser.Quit();
        }
    }
}