﻿using System;
using WebDriverFeatures.Pages.Onliner;
using Xunit;

namespace WebDriverFeatures.Tests
{
    public class MultiWindow : BaseTest
    {
        private readonly string TVCatalogPageUrl = "https://catalog.onliner.by/tv/";
        private readonly string GooglePlayAppName = "Каталог Onliner";
        [Fact]
        public void MultiWindowTest()
        {
            Browser.OpenURL(TVCatalogPageUrl);
            TVCatalogPage tvCatalogPage = new TVCatalogPage(Browser);
            AppStoreAppPage appStoreAppPage = tvCatalogPage.OpenAppPageInAppleStore(tvCatalogPage.AppleStoreAppLink);
            Browser.SwitchToOriginalWindow();
            GooglePlayAppPage googlePlayAppPage = tvCatalogPage.OpenAppPageInGooglePlay(tvCatalogPage.GooglePlayAppLink);
            Assert.Equal(3, Browser.GetWindowHandlesCount());
            Browser.SwitchToNextWindow();
            Assert.Equal(GooglePlayAppName, googlePlayAppPage.GetAppName());
            GooglePlaySimilarAppsPage googlePlaySimilarAppsPage = googlePlayAppPage.GoToSimilarAppsPage();
            Console.WriteLine(googlePlaySimilarAppsPage.GetSimilarAppsCount());
            googlePlaySimilarAppsPage.Close();
            Browser.SwitchToOriginalWindow();
            Browser.SwitchToNextWindow();
            appStoreAppPage.MoreDescriptionButton.Click();
            appStoreAppPage.Close();
            Browser.SwitchToOriginalWindow();
            tvCatalogPage.AdsLink.Click();
        }
    }
}
