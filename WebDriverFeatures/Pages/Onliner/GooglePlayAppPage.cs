﻿using OpenQA.Selenium;
using WebDriverFeatures.Browsers;


namespace WebDriverFeatures.Pages.Onliner
{
    class GooglePlayAppPage : BasePage
    {
        public GooglePlayAppPage(Browser browser) : base(browser) { }

        #region Elements

        public IWebElement AppName => Browser.FindElement(By.XPath("//h1[@class='AHFaub']/span"));
        public IWebElement MoreSimilarAppsButton => Browser.FindElement(By.XPath("//div[@class='W9yFB']//a[text()='Ещё'|text()='See more']"));
        
        #endregion

        #region Methods

        public GooglePlaySimilarAppsPage GoToSimilarAppsPage()
        {
            MoreSimilarAppsButton.Click();
            return new GooglePlaySimilarAppsPage(Browser);
        }

        public string GetAppName() => AppName.Text;


        #endregion
    }
}
