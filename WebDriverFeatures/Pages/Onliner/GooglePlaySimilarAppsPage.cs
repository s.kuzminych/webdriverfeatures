﻿using OpenQA.Selenium;
using System.Collections.Generic;
using WebDriverFeatures.Browsers;

namespace WebDriverFeatures.Pages.Onliner
{
    class GooglePlaySimilarAppsPage : BasePage
    {
        public GooglePlaySimilarAppsPage(Browser browser) : base(browser) { }

        public List<IWebElement> SimilarAppElements => Browser.FindElements(By.XPath("//div[@class='ZmHEEd ']/*"));

        public string GetSimilarAppsCount() => SimilarAppElements.Count.ToString();
    }
}
