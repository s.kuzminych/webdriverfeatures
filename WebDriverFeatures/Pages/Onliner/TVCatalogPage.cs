﻿using OpenQA.Selenium;
using System.Collections.Generic;
using WebDriverFeatures.Browsers;

namespace WebDriverFeatures.Pages.Onliner
{
    class TVCatalogPage : BasePage
    {
        public TVCatalogPage(Browser browser) : base(browser) { }

        #region Elements

        public IWebElement AppleStoreAppLink => Browser.FindElement(By.XPath("//div[@id='schema-filter']//a[contains(@class, 'store-item_apple')]"));
        public IWebElement GooglePlayAppLink => Browser.FindElement(By.XPath("//div[@id='schema-filter']//a[contains(@class, 'store-item_google')]"));
        public IWebElement AdsLink => Browser.FindElement(By.XPath("//header[@class='g-top']"));
        public List<IWebElement> CompareCheckboxes => Browser.FindElements(By.XPath("//div[@class='schema-product']/div[contains(@class, 'schema-product__part_1')]//label"));
        public IWebElement CompareButton => Browser.FindElement(By.XPath("//a[contains(@href, 'catalog.onliner.by/compare/')]"));

        #endregion

        #region Methods

        public AppStoreAppPage OpenAppPageInAppleStore(IWebElement appleStorePageLink)
        {
            appleStorePageLink.Click();
            Browser.SwitchToNextWindow();
            return new AppStoreAppPage(Browser);
            
        }
        public GooglePlayAppPage OpenAppPageInGooglePlay(IWebElement googlePlayPageLink)
        {
            googlePlayPageLink.Click();
            Browser.SwitchToNextWindow();
            return new GooglePlayAppPage(Browser);
        }
        public ComparePage GoToComparePage()
        {
            CompareButton.Click();
            return new ComparePage(Browser);
        }

        #endregion
    }
}
