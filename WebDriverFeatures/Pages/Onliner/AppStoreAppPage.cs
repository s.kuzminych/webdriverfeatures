﻿using OpenQA.Selenium;
using WebDriverFeatures.Browsers;

namespace WebDriverFeatures.Pages.Onliner
{
    class AppStoreAppPage : BasePage
    {
        public AppStoreAppPage(Browser browser) : base(browser) { }

        public IWebElement MoreDescriptionButton => Browser.FindElement(By.XPath("//div[@class='section__description']//button[text()='more']"));

        
    }
}
