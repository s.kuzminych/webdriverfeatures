﻿using OpenQA.Selenium;
using System.Collections.Generic;
using WebDriverFeatures.Browsers;

namespace WebDriverFeatures.Pages.Onliner
{
    class ComparePage : BasePage
    {
        public ComparePage(Browser browser) : base(browser) { }

        #region Elements

        public IWebElement CompareButton => Browser.FindElement(By.XPath("//a[contains(@href, 'catalog.onliner.by/compare/')]"));
        public IWebElement SizeParameterCell => Browser.FindElement(By.XPath("//td[@class='product-table__cell']/span[text()='Диагональ экрана']"));
        public IWebElement SizeParameterTip => Browser.FindElement(By.XPath("//span[text()='Диагональ экрана']/following-sibling::*/span"));
        public List<IWebElement> DeleteElementButtons => Browser.FindElements(By.XPath("//th//a[@title='Удалить']"));
        public IWebElement Tip => Browser.FindElement(By.Id("productTableTip"));
        public IWebElement Title => Browser.FindElement(By.XPath("//h1"));

        #endregion

        public void MoveMouseToElement(IWebElement targetElement) => Browser.MoveToElement(targetElement);

        public void WaitUntilElementIsDisplayed(IWebElement targetElement) => Browser.WaitUntilElementIsDisplayed(targetElement);

        public void WaitUntilElementIsNotDisplayed(IWebElement targetElement) => Browser.WaitUntilElementIsNotDisplayed(targetElement);
    }
}

