﻿using WebDriverFeatures.Browsers;

namespace WebDriverFeatures.Pages
{
    public abstract class BasePage
    {
        protected Browser Browser { get; }
        protected BasePage(Browser browser)
        {
            Browser = browser;
        }
        public void Close()
        {
            Browser.Close();
        }
    }
}
