# WebDriverFeatures
### Alerts management:
- [AlertsTest Method](https://gitlab.com/s.kuzminych/webdriverfeatures/blob/master/WebDriverFeatures/Tests/Alerts.cs)
***
### Multiple Windows management:
- [MultiWindowTest Method](https://gitlab.com/s.kuzminych/webdriverfeatures/blob/master/WebDriverFeatures/Tests/MultiWindow.cs)
***
### Frames switching:
- [FramesTest Method](https://gitlab.com/s.kuzminych/webdriverfeatures/blob/master/WebDriverFeatures/Tests/Frames.cs)
***
### Waits using:
- [WaitsTest Method](https://gitlab.com/s.kuzminych/webdriverfeatures/blob/master/WebDriverFeatures/Tests/Waits.cs)
***
### JavaScriptExecutor and Asserts interaction:
- Part of JavaScriptExecutor task: [Browser.cs](https://gitlab.com/s.kuzminych/webdriverfeatures/blob/master/WebDriverFeatures/Browsers/Browser.cs) contains methods which are using JavaScriptExecutor: ScrollToTop, ScrollToBottom, SendKeys and Click.
- Part of Asserts task: There is a [Assertions.cs](https://gitlab.com/s.kuzminych/webdriverfeatures/blob/master/WebDriverFeatures/Tests/Assertions.cs) that uses Asserts types from task.
